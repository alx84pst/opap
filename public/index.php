<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");

CModule::IncludeModule("iblock");
$arSelect = Array(
  "ID",
  "NAME",
  "PREVIEW_TEXT",
  "DETAIL_PICTURE",
  "DETAIL_TEXT",
  "PROPERTY_FIRST_CAPTION", 
  "PROPERTY_SECOND_CAPTION",
  "PROPERTY_SECOND_CAPT_OFFSET",
  "PROPERTY_LOADER_TEXT",
  "PROPERTY_DRIVE_TEXT",
  "PROPERTY_OFFER_TEXT",
  "PROPERTY_PRICE_OFFER_TEXT",
  "PROPERTY_PRICE_ARC_REPLACE",
  "PROPERTY_C_NAME",
  "PROPERTY_C_DESC",
  "PROPERTY_C_ADDR",
  "PROPERTY_C_TELF",
  "PROPERTY_C_TIME_SERV",
  "PROPERTY_C_TIME_SALE",
  "PROPERTY_S_PERSON_HEADER",
  "PROPERTY_S_PERSON",
  "PROPERTY_S_ADVERT_HEADER",
  "PROPERTY_S_ADVERT"
);
$arFilter = Array("IBLOCK_ID" => $APPLICATION->GetDirProperty("IBLOCK_ID"), "ID" => $APPLICATION->GetDirProperty("LANDING_ID"));
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
$ar_res = $res->GetNext();

printJSON('App.modal.contacts',
  Array(
    'name' => $ar_res["PROPERTY_C_NAME_VALUE"],
    'desc' => $ar_res["PROPERTY_C_DESC_VALUE"],
    'addr' => $ar_res["PROPERTY_C_ADDR_VALUE"],
    'telf' => $ar_res["PROPERTY_C_TELF_VALUE"],
    'time' => array('service' => $ar_res["PROPERTY_C_TIME_SERV_VALUE"], 'sale' => $ar_res["PROPERTY_C_TIME_SALE_VALUE"])
  )
);
printJSON('App.modal.soglasie',
  Array(
    'person' => array('caption' => $ar_res["PROPERTY_S_PERSON_HEADER_VALUE"], 'text' => $ar_res["PROPERTY_S_PERSON_VALUE"]['TEXT']),
    'advert' => array('caption' => $ar_res["PROPERTY_S_ADVERT_HEADER_VALUE"], 'text' => $ar_res["PROPERTY_S_ADVERT_VALUE"]['TEXT'])
  )
);
printJSON('App.price.prohibited', $ar_res["PROPERTY_PRICE_OFFER_TEXT_VALUE"]);
printJSON('App.price.arc_replace', ($ar_res["PROPERTY_PRICE_ARC_REPLACE_VALUE"] === null)?false:true);
printJSON('App.caption.offset', $ar_res["PROPERTY_SECOND_CAPT_OFFSET_VALUE"]);
?>

<div class="container">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-img">
      <img class="img-responsive" src="<?=CFile::GetPath($ar_res["DETAIL_PICTURE"])?>">
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<?
if(
  (isset($_GET['bitrix_include_areas'])
  &&  strtoupper($_GET['bitrix_include_areas'])=='Y') && $USER->IsAuthorized()
) {
  echo "<a class='btn btn-primary btn-lg active' role='button' href='/bitrix/admin/iblock_element_edit.php?WF=Y&ID=".$arFilter['ID']."&type=new_cars&lang=ru&IBLOCK_ID=".$arFilter['IBLOCK_ID']."&find_section_section=0'>Редактировать параметры лэндинга</a>";
}

$APPLICATION->SetPageProperty("title", $ar_res["NAME"]);
if(!empty($ar_res["PROPERTY_FIRST_CAPTION_VALUE"])) {
  echo "<h1 class='page-h1'>".$ar_res["PROPERTY_FIRST_CAPTION_VALUE"]."</h1>";
}
$offset = "col-lg-9 col-lg-offset-3 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3 col-xs-12 col-sx-offset-0";
if(!empty($ar_res["PROPERTY_SECOND_CAPT_OFFSET_VALUE"]) && $ar_res["PROPERTY_SECOND_CAPT_OFFSET_VALUE"] != 0) {
  if($ar_res["PROPERTY_SECOND_CAPT_OFFSET_VALUE"] < 10) {
    $off = $ar_res["PROPERTY_SECOND_CAPT_OFFSET_VALUE"];
    $num = 12 - $off;
    $offset = "col-lg-".$num." col-lg-offset-".$off." col-md-".$num." col-md-offset-".$off." col-sm-".$num." col-sm-offset-".$off." col-xs-12 col-sx-offset-0";
  } 
}
if(!empty($ar_res["PROPERTY_SECOND_CAPT_OFFSET_VALUE"]) && $ar_res["PROPERTY_SECOND_CAPT_OFFSET_VALUE"] == 0) {
  $offset = "col-lg-12 col-md-12 col-sm-12 col-xs-12";
}
?>
    </div>
    <div class="<?=$offset?>">
<?
if(!empty($ar_res["PROPERTY_SECOND_CAPTION_VALUE"])) {
  echo "<h2 class='page-h1'>".$ar_res["PROPERTY_SECOND_CAPTION_VALUE"]."</h2>";
}
?>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="page-text">
<?
if(!empty($ar_res["DETAIL_TEXT"])) {
  echo $ar_res["DETAIL_TEXT"];
}
?>
      </div>
    </div>
  </div>
</div>

<div id=app></div>

<div class="container">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="disclaimer">
<?
if(!empty($ar_res["PREVIEW_TEXT"])) {
  echo $ar_res["PREVIEW_TEXT"];
}
?>
      </div>
    </div>
  </div>
</div>

<?$APPLICATION->AddHeadScript('js/app.b49f9322.js');?>

<?$APPLICATION->AddHeadScript('js/chunk-vendors.7cc66ca8.js');?>

<?$APPLICATION->SetAdditionalCSS('/lp/used/dist/css/app.1f1f2d12.css')?>

<?$APPLICATION->SetAdditionalCSS('/lp/used/dist/css/chunk-vendors.085b57b7.css')?>

<?$APPLICATION->SetAdditionalCSS('/lp/used/dist/css/style.css')?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
