export default {
    options: null,
    // Список а/м из Бухты  
    cars: [],
    // отображение процесса загрузки
    loader: true,
    // Количество показываемых а/м
    show: 9,
    // Увеличение показываемых а/м
    increse: 9,
    // Имя
    user_f_name: '',
    // Фамилия
    user_l_name: '',
    // Телефон
    user_phone: '',
    // Почта
    user_email: '',
    // Интересующий автомобиль
    user_interest: '',
    // Сообщение
    user_message: '',
    // Согласие на обработку персональных данных
    user_pers_ready: false,
    // Согласие на получение информации об акциях
    user_advt_ready: false
}