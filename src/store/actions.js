import { HTTP } from '../http'

export const getCarList = (state) => {
    HTTP.post('./cars.php')
        .then(response => {
            state.commit('setCarsList', response.data['ITEMS'])
        })
        .catch(function() {
            //console.log(error)
        })
        .then(function() {
            state.commit('setLoader', false)
        })
}
export const getOptions = (state) => {
    HTTP.post('./landing.php')
        .then(response => {
            state.commit('setOptions', response.data)
        })
        .catch(function() {
            //console.log(error)
        })
        .then(function() {
            state.commit('setLoader', false)
        })
}