<?header('Access-Control-Allow-Origin: http://192.168.1.24:8080');?>
<?header('Access-Control-Allow-Methods: POST');?>
<?header('Content-type: application/json');?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
/**
 * @author Pustynnyy Aleksandr <apjobs@yandex.ru>
 * 
 * Страница для вывода списка а/м с пробегом
 * с помощью вызова процедуры CCabinetService::GetCarBuyUsed2LandOffers()
 * без параметров
 * 
 * @return Список а/м с пробегом
 * @return json
 */
$module = 'car.buy.images';

CModule::IncludeModule("lv_cabinet");

$request = CCabinetService::GetCarBuyUsed2LandOffers();

$arResult['ITEMS'] = Array();


// Перебираем ответ процедуры
foreach ($request as $i => $arItem) {
  // var_dump($arItem);
  // Путь к хранилищу изображений а/м
  $path = "/upload/". $module ."/". $arItem['id'];
  // Абсолютный путь
  $absp = getAbsolutePath($path);
  // получаем список содержимого по указанному пути
  $files = glob($absp . "/*");
  // Обнуляем значение
  $arItem['image'] = array();
  // Получаем дату последнего изменения изображений в базе
  $filedate = date_parse_from_format('Y-m-d H:i:s',$arItem['image_changedate']);
  // var_dump($filedate);
  // Приводим дату к абсолютной величине (UTM)
  $filedate = mktime($filedate['hour'], $filedate['minute'], $filedate['second'], $filedate['month'], $filedate['day'], $filedate['year']);
  // Сравниваем дату создания первого фала и дату из базы
  // var_dump(filemtime($files[0]). " $ " .$filedate);
  // var_dump(filemtime($files[0]));
  // var_dump($arItem['id']. " - " .filemtime($files[0]));
  if (filemtime($files[0]) > $filedate) {
    // Собираем ссылки в массив
    foreach ($files as $key => $file) {
      $arItem['image'][] = array("id" => $key, "image" => "background-image:url('http://online.audi-peterburg.acp.lan". $path ."/". basename($file) ."');", "class" => "car-image__indicator");
    }
    // exit;
  } else {
    // Удаляем все файлы
    foreach ($files as $file) {
      unlink($file);
    }
  }
  // Если массив пуст
  if (count($arItem['image']) == 0) {
    // Получаем изображения из базы
    $res = CCabinetFile::GetAny($arItem['id'], $module);
    foreach ($res as $key => $img) {
      $arItem['image'][] = array("id" => $key, "image" => "background-image:url('http://online.audi-peterburg.acp.lan".$img."');", "class" => "car-image__indicator");
    }
  }
  
  if (count($arItem['image']) == 0) {
    $arItem['image'][] = "background: #f9f9f9;";
  }

  /**
   * Замена дефиса на длинный дефис в описании конфигурации а/м
   */
  $arItem['configuration'] = preg_replace('/ - /', ' – ', $arItem['configuration']);

  $arItem['configuration'] = explode('; ', $arItem['configuration']);
  foreach ($arItem['configuration'] as $key => $value) {
    if (empty($value)) {
      $arItem['configuration'][$key] = html_entity_decode('&nbsp;');
    }
  }

  $arItem['official_price'] = price_format($arItem['price']);
  $arItem['special_price'] = price_format($arItem['special_price']);
  $arResult['ITEMS'][] = $arItem;
}

/**
 * @param int $num сумма
 * @return string цена в формате 1 100 000
 */
function price_format($num) {
  return number_format($num, 0, ',', ' ');
}

echo(json_encode($arResult));
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>