export const getOptions = state => state.options
export const getCarList = state => state.cars
export const getCarListCount = state => state.cars.length
export const getCarListShow = state => num => {
    return state.cars.filter(function(car, i) {
        return ('name' in car) ? i < num : ''
    })
}
export const getCarInterest = state => id => {
    return state.cars.filter(car => car.id === id)
}
export const getCountShow = state => state.show
export const getIncreseShow = state => state.increse
export const getLoader = state => state.loader
export const getUserFirstName = state => state.user_f_name
export const getUserLastName = state => state.user_l_name
export const getUserPhone = state => state.user_phone
export const getUserEmail = state => state.user_email
export const getUserInterest = state => state.user_interest
export const getUserMessage = state => state.user_message
export const getUserPersReady = state => state.user_pers_ready
export const getUserAdvtReady = state => state.user_advt_ready