<?header('Access-Control-Allow-Origin: http://192.168.1.24:8080');?>
<?header('Access-Control-Allow-Methods: POST');?>
<?header('Content-type: application/json');?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
/**
 * @author Pustynnyy Aleksandr <apjobs@yandex.ru>
 * 
 * Страница для вывода списка а/м с пробегом
 * с помощью вызова процедуры CCabinetService::GetCarBuyUsed2LandOffers()
 * без параметров
 * 
 * @return Список а/м с пробегом
 * @return json
 */
CModule::IncludeModule("lv_cabinet");

$request = CCabinetService::GetCarBuyUsed2LandOffers();

$arResult['ITEMS'] = Array();

foreach ($request as $arItem) {
  /**
   * Замена дефиса на длинный дефис в описании конфигурации а/м
   */
  $arItem['configuration'] = preg_replace('/ - /', ' – ', $arItem['configuration']);
  $arItem['official_price'] = price_format($arItem['price']);
  $arItem['special_price'] = price_format($arItem['special_price']);
  $arResult['ITEMS'][] = $arItem;
}

/**
 * @param int $num сумма
 * @return string цена в формате 1 100 000
 */
function price_format($num) {
  return number_format($num, 0, ',', ' ');
}

echo(json_encode($arResult));
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>